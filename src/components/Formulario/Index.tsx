import React, { useState } from 'react';
import { ITarefa } from '../../types/tarefa';
import Botao from '../Botao/Index';
import style from './Formulario.module.scss'
import { v4 as uuidv4 } from 'uuid'

interface Props{
    setTarefas: React.Dispatch<React.SetStateAction<ITarefa[]>>
}

function Formulario({ setTarefas } : Props){

    const [tarefa, setTarefa] = useState('');
    const [duracao, setDuracao] = useState('00:00');

    function adicionarTarefa(evento: React.FormEvent<HTMLFormElement>){
        evento.preventDefault();
        setTarefas(tarefas => 
            [ 
                ...tarefas , 
                { 
                    tarefa,
                    duracao,
                    selecionado: false,
                    completado: false,
                    id: uuidv4()
                }
            ]
        )
        setTarefa('');
        setDuracao('00:00')
        
    }

    return(
        <form className={style.novaTarefa} onSubmit={adicionarTarefa}>
            <div className={style.inputContainer}>
                <label htmlFor='tarefa'>
                    Add a new Subject
                </label>
                <input 
                    type='text'
                    name='tarefa'
                    id='tarefa'
                    value={tarefa}
                    onChange={e => setTarefa(e.target.value)}
                    placeholder='Subject'
                    required/>
            </div>
            <div className={style.inputContainer}>
                <label htmlFor='tempo'>
                    Timer
                </label>
                <input type='time'
                        step='1'
                        name='tempo'
                        value={duracao}
                        onChange={e => setDuracao(e.target.value)}
                        id='tempo'
                        min='00:00:00'
                        max='01:30:00'
                        required/>
            </div>
            <Botao type='submit'>
                ADD
            </Botao>
        </form>
    )
}


export default Formulario;