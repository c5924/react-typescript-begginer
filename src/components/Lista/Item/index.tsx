import { ITarefa } from '../../../types/tarefa'
import style from './Item.module.scss'

interface Props extends ITarefa {
    selecionaTarefa: (tarefaSelecioanda: ITarefa) => void 
}

export default function Item(
    {
        tarefa,
        duracao,
        selecionado,
        completado,
        id, 
        selecionaTarefa
    } : Props){
    return (
        <li 
            className={`${style.item} ${selecionado ? style.itemSelecionado : ''} ${completado ? style.itemCompletado : ''} `} 
            onClick={() => !completado && selecionaTarefa(
                {
                    tarefa,
                    duracao,
                    selecionado,
                    completado,
                    id
                })}>
            <h3>
                {tarefa}
            </h3>
            <span>
                {duracao}
            </span>
            {
                completado && <span className={style.concluido} aria-label='Tarefa Completada'></span>
            }
        </li>
    )
}