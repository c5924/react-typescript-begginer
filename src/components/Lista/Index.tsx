import style from './Lista.module.scss';
import Item from './Item';
import { ITarefa } from '../../types/tarefa'

interface Props {
    tarefas:ITarefa[],
    selecionaTarefa: (tarefaSelecioanda: ITarefa) => void 
}

function Lista( {tarefas,selecionaTarefa} : Props){

    return(
        <aside className={style.listaTarefas}>
            <h2>
                Today's Studies
            </h2>
            <ul>
                {tarefas.map((_,i) => (
                    <Item 
                        selecionaTarefa={selecionaTarefa}
                        key={i} {... _} />
                ))}
            </ul>
        </aside>
    )
}

export default Lista;