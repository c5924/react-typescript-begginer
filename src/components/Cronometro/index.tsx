import React, {useState, useEffect} from 'react'
import Botao from "../Botao/Index"
import Relogio from "./Relogio"
import style from './Cronometro.module.scss'
import { tempoParaSegundos } from "../../common/utils/time"
import { ITarefa } from "../../types/tarefa"

interface Props{
    selecionado: ITarefa | undefined,
    finalizarTarefa: () => void
}

export default function Cronometro({selecionado,finalizarTarefa} : Props){

    const [tempo, setTempo] = useState<number>();

    useEffect(() => {

        if(selecionado?.duracao){
            setTempo(tempoParaSegundos(selecionado.duracao))
        }
    
    }, [selecionado])
    
    function regressiva(contador: number = 0){
        setTimeout(() => {
            if(contador > 0){
                setTempo(contador - 1)
                return regressiva(contador - 1)
            }
            finalizarTarefa();
        }, 1000);
    }

    return (
        <div className={style.cronometro}>
            <p className={style.titulo}>
                Pick a subject and start the timer
            </p>
            <div className={style.relogioWrapper}>
                <Relogio tempo={tempo} />
            </div>
            <Botao onClick={() => regressiva(tempo)}>
                Start
            </Botao>
        </div>
    )
}