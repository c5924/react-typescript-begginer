export interface ITarefa{
    tarefa:string,
    duracao:string,
    selecionado: boolean,
    completado: boolean,
    id: string
}